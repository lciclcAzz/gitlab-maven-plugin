package io.ultreia.maven.gitlab;

/*-
 * #%L
 * Gitlab Maven Plugin
 * %%
 * Copyright (C) 2017 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.gitlab.api.models.GitlabIssue;
import org.gitlab.api.models.GitlabMilestone;
import org.gitlab.api.models.GitlabProject;
import org.nuiton.version.Versions;

import java.io.IOException;
import java.io.Reader;
import java.io.Writer;
import java.lang.reflect.Type;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.*;

/**
 * Created by tchemit on 20/02/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class GitlabCache {

    private final Path cachePath;
    private final Gson gson;

    GitlabCache(Path cachePath) {
        this.cachePath = cachePath;
        this.gson = new Gson();
    }

    public Optional<GitlabProject> getProject() throws IOException {
        Path projectPath = cachePath.resolve("project.json");
        if (Files.exists(projectPath)) {
            try (Reader reader = Files.newBufferedReader(projectPath)) {
                GitlabProject gitlabMilestone = gson.fromJson(reader, GitlabProject.class);
                return Optional.of(gitlabMilestone);
            }
        }
        return Optional.empty();
    }

    void setProject(GitlabProject gitlabProject) throws IOException {

        Path projectPath = cachePath.resolve("project.json");
        Files.createDirectories(projectPath.getParent());
        try (Writer reader = Files.newBufferedWriter(projectPath)) {
            gson.toJson(gitlabProject, reader);
        }
    }


    Optional<GitlabMilestoneExt> getMilestone(String id) throws IOException {
        Path milestonePath = cachePath.resolve("milestones").resolve(id + ".json");
        if (Files.exists(milestonePath)) {
            try (Reader reader = Files.newBufferedReader(milestonePath)) {
                GitlabMilestoneExt gitlabMilestone = gson.fromJson(reader, GitlabMilestoneExt.class);
                return Optional.of(gitlabMilestone);
            }
        }
        return Optional.empty();
    }

    public List<GitlabMilestoneExt> getMilestones() throws IOException {

        Path milestonesPath = cachePath.resolve("milestones");
        Set<Path> paths = getPaths(milestonesPath);

        LinkedList<GitlabMilestoneExt> result = new LinkedList<>();
        for (Path path : paths) {
            if (path.toFile().getName().endsWith("-issues.json")) {
                continue;
            }
            try (Reader reader = Files.newBufferedReader(path)) {
                GitlabMilestoneExt gitlabMilestone = gson.fromJson(reader, GitlabMilestoneExt.class);
                result.add(gitlabMilestone);
            }
        }
        result.sort(Comparator.comparing(m -> Versions.valueOf(m.getTitle())));
        return result;
    }

    void setMilestone(GitlabMilestone gitlabMilestone) throws IOException {

        Path milestonePath = cachePath.resolve("milestones").resolve(gitlabMilestone.getId() + ".json");
        Files.createDirectories(milestonePath.getParent());
        try (Writer reader = Files.newBufferedWriter(milestonePath)) {
            gson.toJson(gitlabMilestone, reader);
        }
    }

    public Set<GitlabIssue> getMilestoneIssues(GitlabMilestone gitlabMilestone) throws IOException {

        Path issuesPath = cachePath.resolve("milestones").resolve(gitlabMilestone.getId() + "-issues.json");
        try (Reader reader = Files.newBufferedReader(issuesPath)) {
            Type typeOfT = new TypeToken<Set<GitlabIssue>>() {
            }.getType();
            return gson.fromJson(reader, typeOfT);
        }
    }

    void setMilestoneIssues(GitlabMilestone gitlabMilestone, Iterable<GitlabIssue> gitlabIssues) throws IOException {

        Path issuesPath = cachePath.resolve("milestones").resolve(String.format("%d-issues.json", gitlabMilestone.getId()));
        Files.createDirectories(issuesPath.getParent());
        try (Writer reader = Files.newBufferedWriter(issuesPath)) {
            gson.toJson(gitlabIssues, reader);
        }

    }


    private Set<Path> getPaths(Path path) throws IOException {
        Set<Path> paths = new LinkedHashSet<>();
        Files.walkFileTree(path, new SimpleFileVisitor<Path>() {

            @Override
            public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                if (file.toFile().getName().endsWith(".json")) {
                    paths.add(file);
                }
                return FileVisitResult.CONTINUE;
            }
        });
        return paths;
    }

}
