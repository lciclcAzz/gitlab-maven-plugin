package io.ultreia.maven.gitlab;

/*-
 * #%L
 * Gitlab Maven Plugin
 * %%
 * Copyright (C) 2017 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.github.mustachejava.Mustache;
import io.ultreia.maven.gitlab.model.ProjectModel;
import io.ultreia.maven.gitlab.model.ProjectModelBuilder;
import io.ultreia.maven.gitlab.model.ReleaseArtifactModel;
import org.apache.maven.plugins.annotations.Execute;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;

import java.io.File;
import java.io.IOException;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.List;

/**
 * Generate a {@code CHANGELOG.md} file with releases + stagin data.
 * <p>
 * Created by tchemit on 06/03/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
@Mojo(name = "generate-staging-changelog", defaultPhase = LifecyclePhase.GENERATE_RESOURCES)
@Execute(goal = "download-milestones")
public class GenerateStagingChangelogMojo extends GitlabMojoSupport {

    @Parameter(property = "gitlab.milestone", required = true)
    private String milestone;

    @Parameter(property = "gitlab.changelogFile", required = true, defaultValue = "${project.basedir}/CHANGELOG.md")
    private File changelogFile;

    @Parameter(property = "gitlab.changesTitle", required = true)
    private String changesTitle;

    @Parameter(property = "gitlab.changesAuthor", required = true)
    private String changesAuthor;

    @Parameter(property = "gitlab.changesAuthorEmail", required = true)
    private String changesAuthorEmail;

    /**
     * Comma separated tracker names (they are gitlab label).
     * <p>
     * <strong>Note:</strong> order matter (issues will be group by each tracker).
     */
    @Parameter(property = "gitlab.trackers", required = true)
    private String trackers;

    @Parameter
    private List<ReleaseArtifactModel> releaseArtifacts;

    @Parameter(property = "gitlab.stagingUrl", required = true)
    private String stagingUrl;

    @Parameter(property = "gitlab.skip")
    private boolean skip;

    @Override
    boolean isSkip() {
        return skip || shouldSkip();
    }

    @Override
    protected void execute(GitlabAPIExt api) throws IOException, ProjectNotFoundException {

        ProjectModel model = ProjectModelBuilder.create(this)
                .setAuthor(changesAuthor)
                .setAuthorEmail(changesAuthorEmail)
                .setTitle(changesTitle)
                .setTrackers(Arrays.asList(trackers.split("\\s*,\\s*")))
                .setReleaseArtifacts(releaseArtifacts)
                .setMilestone(milestone)
                .setStaging(true)
                .setStagingUrl(stagingUrl)
                .build();

        Mustache mustache = getMustache("CHANGELOG.md.mustache");

        try (Writer writer = Files.newBufferedWriter(changelogFile.toPath(), StandardCharsets.UTF_8)) {
            mustache.execute(writer, model).flush();
        }

        getLog().info(String.format("CHANGELOG.md generated to %s", changelogFile.getParent()));

    }

}
