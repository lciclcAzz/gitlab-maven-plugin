package io.ultreia.maven.gitlab;

/*-
 * #%L
 * Gitlab Maven Plugin
 * %%
 * Copyright (C) 2017 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import io.ultreia.maven.gitlab.model.GitlabIssueTime;
import org.gitlab.api.GitlabAPI;
import org.gitlab.api.models.GitlabIssue;
import org.gitlab.api.models.GitlabMilestone;
import org.gitlab.api.models.GitlabProject;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;

/**
 * Created by tchemit on 20/02/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class GitlabAPIExt {

    private final GitlabAPI delegate;

    public GitlabAPIExt(GitlabAPI delegate) {
        this.delegate = delegate;
    }

    public GitlabProject getProject(Serializable projectId) throws IOException {
        return delegate.getProject(projectId);
    }

    public List<GitlabMilestoneExt> getMilestones(GitlabProject project) throws IOException {
        String tailUrl = GitlabProject.URL + "/" + project.getId() + GitlabMilestone.URL;
        List<GitlabMilestoneExt> milestones = delegate.retrieve().getAll(tailUrl, GitlabMilestoneExt[].class);
        milestones.sort(GitlabMilestoneExt::compareTo);
        return milestones;
    }

    public List<GitlabIssue> getMilestoneIssues(GitlabProject project, GitlabMilestone milestone) throws IOException {

        String tailUrl = GitlabProject.URL + "/" + project.getId() + GitlabMilestone.URL + "/" + milestone.getId() + GitlabIssue.URL;
        return delegate.retrieve().getAll(tailUrl, GitlabIssue[].class);

    }

    public GitlabMilestone closeMilestone(GitlabMilestone edited) throws IOException {
        return delegate.updateMilestone(edited, "close");
    }

    public GitlabMilestone createMilestone(GitlabProject project, String title) throws IOException {
        return delegate.createMilestone(project.getId(), title, null, null);
    }

    public GitlabIssueTime getIssueTime(GitlabProject project, Integer issueId) throws IOException {
        String tailUrl = GitlabProject.URL + "/" + project.getId() + String.format(GitlabIssueTime.URL, issueId);
        return delegate.retrieve().to(tailUrl, GitlabIssueTime.class);
    }
}
