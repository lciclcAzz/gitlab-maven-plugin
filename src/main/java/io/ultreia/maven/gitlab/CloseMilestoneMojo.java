package io.ultreia.maven.gitlab;

/*-
 * #%L
 * Gitlab Maven Plugin
 * %%
 * Copyright (C) 2017 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.util.Date;
import org.apache.commons.lang3.StringUtils;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.gitlab.api.models.GitlabMilestone;
import org.gitlab.api.models.GitlabProject;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

/**
 * Get the given milestone and close it. Will fail if the milestone does not exist.
 * <p>
 * Created by tchemit on 12/03/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
@Mojo(name = "close-milestone", defaultPhase = LifecyclePhase.INITIALIZE)
public class CloseMilestoneMojo extends GitlabMojoSupport {

    @Parameter(property = "gitlab.milestone", required = true, defaultValue = "${project.version}")
    private String milestone;

    @Parameter(property = "gitlab.addDueDate")
    private boolean addDueDate;

    @Parameter(property = "gitlab.skip")
    private boolean skip;

    @Override
    boolean isSkip() {
        return skip || shouldSkip();
    }

    @Override
    void execute(GitlabAPIExt api) throws IOException, MilestoneNotFoundException, ProjectNotFoundException {

        if (milestone.endsWith("-SNAPSHOT")) {
            milestone = StringUtils.removeEnd(milestone, "-SNAPSHOT");
        }

        if (isVerbose()) {
            getLog().info("loading project...");
        }
        String projectPath = getGitlabProjectPath();
        GitlabProject gitlabProject = api.getProject(projectPath);

        GitlabCache gitlabCache = newCache();
        gitlabCache.setProject(gitlabProject);

        getLog().info(String.format("GitLab project (%s) id: %d", projectPath, gitlabProject.getId()));
        if (isVerbose()) {
            getLog().info("loading milestones...");
        }
        List<GitlabMilestoneExt> gitlabMilestones = api.getMilestones(gitlabProject);
        getLog().info(String.format("found %d milestone(s).", gitlabMilestones.size()));

        Optional<GitlabMilestoneExt> optionalMilestone = gitlabMilestones.stream().filter(m -> milestone.equals(m.getTitle())).findFirst();
        if (!optionalMilestone.isPresent()) {
            throw new MilestoneNotFoundException(milestone);
        }

        getLog().info(String.format("Closing milestone %s.", optionalMilestone.get().getTitle()));

        if (addDueDate) {
            optionalMilestone.get().setDueDate(new Date());
        }
        api.closeMilestone(optionalMilestone.get());


    }
}
