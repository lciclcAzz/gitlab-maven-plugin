# GitLab Maven plugin

 * Author [Tony Chemit](mailto:dev@tchemit.fr)
 * Last generated at 2017-04-12 05:17.

## Version [1.0.9](https://gitlab.com/ultreiaio/gitlab-maven-plugin/milestones/11)

**Closed at *In progress*.**


### Issues
  * [[Feature 24]](https://gitlab.com/ultreiaio/gitlab-maven-plugin/issues/24) **Add a new flag *addDueToDate* to fill dueToDate when closing milestone** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Feature 25]](https://gitlab.com/ultreiaio/gitlab-maven-plugin/issues/25) **Add a new mojo to compute milestone estimate time** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.0.8](https://gitlab.com/ultreiaio/gitlab-maven-plugin/milestones/10)

**Closed at *In progress*.**


### Issues
  * [[Feature 20]](https://gitlab.com/ultreiaio/gitlab-maven-plugin/issues/20) **up to pom 5** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Task 21]](https://gitlab.com/ultreiaio/gitlab-maven-plugin/issues/21) **update to pom 7** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Task 22]](https://gitlab.com/ultreiaio/gitlab-maven-plugin/issues/22) **migrate project to gitlab.com/ultreiaio/gitlab-maven-plugin** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Task 23]](https://gitlab.com/ultreiaio/gitlab-maven-plugin/issues/23) **add site (see https://ultreiaio.gitlab.io/gitla-maven-plugin)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.0.7](https://gitlab.com/ultreiaio/gitlab-maven-plugin/milestones/9)

**Closed at 2017-03-12.**


### Issues
  * [[Bug 19]](https://gitlab.com/ultreiaio/gitlab-maven-plugin/issues/19) **Sort milestones using version natural order** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Feature 15]](https://gitlab.com/ultreiaio/gitlab-maven-plugin/issues/15) **Add a close-milestone mojo** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Feature 16]](https://gitlab.com/ultreiaio/gitlab-maven-plugin/issues/16) **Add a check-milestone-exist** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Feature 17]](https://gitlab.com/ultreiaio/gitlab-maven-plugin/issues/17) **Add create milestone mojo** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Task 18]](https://gitlab.com/ultreiaio/gitlab-maven-plugin/issues/18) **Update pom to version 4** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.0.6](https://gitlab.com/ultreiaio/gitlab-maven-plugin/milestones/8)

**Closed at *In progress*.**


### Issues
  * [[Bug 14]](https://gitlab.com/ultreiaio/gitlab-maven-plugin/issues/14) **Bad milestone usage in generate-staging-changelog** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.0.5](https://gitlab.com/ultreiaio/gitlab-maven-plugin/milestones/7)

**Closed at *In progress*.**


### Issues
  * [[Feature 13]](https://gitlab.com/ultreiaio/gitlab-maven-plugin/issues/13) **Add generate-staging-changelog mojo** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.0.4](https://gitlab.com/ultreiaio/gitlab-maven-plugin/milestones/6)

**Closed at 2017-02-21.**


### Issues
  * [[Feature 11]](https://gitlab.com/ultreiaio/gitlab-maven-plugin/issues/11) **Improve trackers usage in generate mojo configuration** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Feature 12]](https://gitlab.com/ultreiaio/gitlab-maven-plugin/issues/12) **Add staging mode for changelog generation** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.0.3](https://gitlab.com/ultreiaio/gitlab-maven-plugin/milestones/5)

**Closed at 2017-02-20.**


### Issues
  * [[Feature 8]](https://gitlab.com/ultreiaio/gitlab-maven-plugin/issues/8) **Add deployed artifacts for each release into generated changelog** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Feature 9]](https://gitlab.com/ultreiaio/gitlab-maven-plugin/issues/9) **show only closed milestones in generated changelog** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Feature 10]](https://gitlab.com/ultreiaio/gitlab-maven-plugin/issues/10) **Improve issues display in generated changelog** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.0.2](https://gitlab.com/ultreiaio/gitlab-maven-plugin/milestones/4)

**Closed at 2017-02-20.**


### Issues
  * [[Bug 5]](https://gitlab.com/ultreiaio/gitlab-maven-plugin/issues/5) **Fix changelog template (missing a line after each milestone)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Feature 6]](https://gitlab.com/ultreiaio/gitlab-maven-plugin/issues/6) **Add milestone link in generated changelog** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Feature 7]](https://gitlab.com/ultreiaio/gitlab-maven-plugin/issues/7) **Improve date format in generated changelog** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.0.1](https://gitlab.com/ultreiaio/gitlab-maven-plugin/milestones/3)

**Closed at 2017-02-20.**


### Issues
  * [[Bug 4]](https://gitlab.com/ultreiaio/gitlab-maven-plugin/issues/4) **Fix changelog template (missing a line after each tracker)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.0.0](https://gitlab.com/ultreiaio/gitlab-maven-plugin/milestones/1)

**Closed at 2017-02-20.**


### Issues
  * [[Feature 1]](https://gitlab.com/ultreiaio/gitlab-maven-plugin/issues/1) **Add download-milestones mojo** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Feature 2]](https://gitlab.com/ultreiaio/gitlab-maven-plugin/issues/2) **Add generate-changes mojo** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Feature 3]](https://gitlab.com/ultreiaio/gitlab-maven-plugin/issues/3) **Add generate-changelog mojo** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

